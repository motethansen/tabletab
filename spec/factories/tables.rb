# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :table do
    tablenumber 1
    tablename "MyString"
    restaurant_id 1
  end
end
