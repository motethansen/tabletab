# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :restaurant do
    name "MyString"
    street "MyString"
    city "MyString"
    state "MyString"
    zip "MyString"
    country "MyString"
    phone "MyString"
    restaurant_size "MyString"
    number_of_staff_pr_shift 1
    restaurant_type "MyString"
    franchise false
    user_id 1
  end
end
