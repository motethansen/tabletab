# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order do
    restaurant_id 1
    table_id 1
    status "MyString"
    create_order_timestamp "2012-11-07 14:47:41"
    produce_order_timestamp "2012-11-07 14:47:41"
    close_order_timestamp "2012-11-07 14:47:41"
  end
end
