# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :menu do
    title "MyString"
    description "MyString"
    catagory "MyString"
    price "9.99"
    image_id 1
    happy_hour_from "2012-11-02 17:06:18"
    happy_hour_to "2012-11-02 17:06:18"
    happy_hour_price "9.99"
    restaurant_id 1
    display_on_mobile_menu false
  end
end
