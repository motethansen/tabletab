require 'spec_helper'

describe "restaurants/show" do
  before(:each) do
    @restaurant = assign(:restaurant, stub_model(Restaurant,
      :name => "Name",
      :street => "Street",
      :city => "City",
      :state => "State",
      :zip => "Zip",
      :country => "Country",
      :phone => "Phone",
      :restaurant_size => "Restaurant Size",
      :number_of_staff_pr_shift => 1,
      :restaurant_type => "Restaurant Type",
      :franchise => false,
      :user_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Street/)
    rendered.should match(/City/)
    rendered.should match(/State/)
    rendered.should match(/Zip/)
    rendered.should match(/Country/)
    rendered.should match(/Phone/)
    rendered.should match(/Restaurant Size/)
    rendered.should match(/1/)
    rendered.should match(/Restaurant Type/)
    rendered.should match(/false/)
    rendered.should match(/2/)
  end
end
