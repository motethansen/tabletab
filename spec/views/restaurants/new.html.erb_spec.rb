require 'spec_helper'

describe "restaurants/new" do
  before(:each) do
    assign(:restaurant, stub_model(Restaurant,
      :name => "MyString",
      :street => "MyString",
      :city => "MyString",
      :state => "MyString",
      :zip => "MyString",
      :country => "MyString",
      :phone => "MyString",
      :restaurant_size => "MyString",
      :number_of_staff_pr_shift => 1,
      :restaurant_type => "MyString",
      :franchise => false,
      :user_id => 1
    ).as_new_record)
  end

  it "renders new restaurant form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => restaurants_path, :method => "post" do
      assert_select "input#restaurant_name", :name => "restaurant[name]"
      assert_select "input#restaurant_street", :name => "restaurant[street]"
      assert_select "input#restaurant_city", :name => "restaurant[city]"
      assert_select "input#restaurant_state", :name => "restaurant[state]"
      assert_select "input#restaurant_zip", :name => "restaurant[zip]"
      assert_select "input#restaurant_country", :name => "restaurant[country]"
      assert_select "input#restaurant_phone", :name => "restaurant[phone]"
      assert_select "input#restaurant_restaurant_size", :name => "restaurant[restaurant_size]"
      assert_select "input#restaurant_number_of_staff_pr_shift", :name => "restaurant[number_of_staff_pr_shift]"
      assert_select "input#restaurant_restaurant_type", :name => "restaurant[restaurant_type]"
      assert_select "input#restaurant_franchise", :name => "restaurant[franchise]"
      assert_select "input#restaurant_user_id", :name => "restaurant[user_id]"
    end
  end
end
