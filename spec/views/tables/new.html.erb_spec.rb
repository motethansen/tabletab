require 'spec_helper'

describe "tables/new" do
  before(:each) do
    assign(:table, stub_model(Table,
      :tablenumber => 1,
      :tablename => "MyString",
      :restaurant_id => 1
    ).as_new_record)
  end

  it "renders new table form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => tables_path, :method => "post" do
      assert_select "input#table_tablenumber", :name => "table[tablenumber]"
      assert_select "input#table_tablename", :name => "table[tablename]"
      assert_select "input#table_restaurant_id", :name => "table[restaurant_id]"
    end
  end
end
