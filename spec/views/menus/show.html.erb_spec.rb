require 'spec_helper'

describe "menus/show" do
  before(:each) do
    @menu = assign(:menu, stub_model(Menu,
      :title => "Title",
      :description => "Description",
      :catagory => "Catagory",
      :price => "9.99",
      :image_id => 1,
      :happy_hour_price => "9.99",
      :restaurant_id => 2,
      :display_on_mobile_menu => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Title/)
    rendered.should match(/Description/)
    rendered.should match(/Catagory/)
    rendered.should match(/9.99/)
    rendered.should match(/1/)
    rendered.should match(/9.99/)
    rendered.should match(/2/)
    rendered.should match(/false/)
  end
end
