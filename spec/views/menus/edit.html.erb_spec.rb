require 'spec_helper'

describe "menus/edit" do
  before(:each) do
    @menu = assign(:menu, stub_model(Menu,
      :title => "MyString",
      :description => "MyString",
      :catagory => "MyString",
      :price => "9.99",
      :image_id => 1,
      :happy_hour_price => "9.99",
      :restaurant_id => 1,
      :display_on_mobile_menu => false
    ))
  end

  it "renders the edit menu form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => menus_path(@menu), :method => "post" do
      assert_select "input#menu_title", :name => "menu[title]"
      assert_select "input#menu_description", :name => "menu[description]"
      assert_select "input#menu_catagory", :name => "menu[catagory]"
      assert_select "input#menu_price", :name => "menu[price]"
      assert_select "input#menu_image_id", :name => "menu[image_id]"
      assert_select "input#menu_happy_hour_price", :name => "menu[happy_hour_price]"
      assert_select "input#menu_restaurant_id", :name => "menu[restaurant_id]"
      assert_select "input#menu_display_on_mobile_menu", :name => "menu[display_on_mobile_menu]"
    end
  end
end
