require 'spec_helper'

describe "menus/index" do
  before(:each) do
    assign(:menus, [
      stub_model(Menu,
        :title => "Title",
        :description => "Description",
        :catagory => "Catagory",
        :price => "9.99",
        :image_id => 1,
        :happy_hour_price => "9.99",
        :restaurant_id => 2,
        :display_on_mobile_menu => false
      ),
      stub_model(Menu,
        :title => "Title",
        :description => "Description",
        :catagory => "Catagory",
        :price => "9.99",
        :image_id => 1,
        :happy_hour_price => "9.99",
        :restaurant_id => 2,
        :display_on_mobile_menu => false
      )
    ])
  end

  it "renders a list of menus" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Catagory".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
