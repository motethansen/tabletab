class MenusController < ApplicationController
  # GET /menus
  # GET /menus.json
  def index
    @menus = Menu.all

    respond_to do |format| 
      format.html # index.html.erb
      format.json { render json: @menus }
    end
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
    @menu = Menu.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @menu }
    end
  end

  # GET /menus/new
  # GET /menus/new.json
  def new
    @menu = Menu.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @menu }
    end
  end

  # GET /menus/1/edit
  def edit
    @menu = Menu.find(params[:id])
  end

  # POST /menus
  # POST /menus.json
  def create
    @menu = Menu.new(params[:menu])

    respond_to do |format|
      if @menu.save
        format.html { redirect_to @menu, notice: 'Menu was successfully created.' }
        format.json { render json: @menu, status: :created, location: @menu }
      else
        format.html { render action: "new" }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /menus/1
  # PUT /menus/1.json
  def update
    @menu = Menu.find(params[:id])

    respond_to do |format|
      if @menu.update_attributes(params[:menu])
        format.html { redirect_to @menu, notice: 'Menu was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu = Menu.find(params[:id])
    @menu.destroy

    respond_to do |format|
      format.html { redirect_to menus_url }
      format.json { head :no_content }
    end
  end


  def cl_form_tag(callback_url, options={}, &block)
    if valid_cloudinary_response?
      #success
    end

    form_options = options.delete(:form) || {}
    form_options[:method] = :post
    form_options[:multipart] = true

    options[:timestamp] = Time.now.to_i
    options[:callback] = callback_url
    if options[:transformation]
       options[:transformation] = Cloudinary::Utils.generate_transformation_string(options[:transformation])
    end

    options[:signature] = Cloudinary::Utils.api_sign_request(options, Cloudinary.config.api_secret)

    options[:api_key] = Cloudinary.config.api_key

    url = "https://api.cloudinary.com/v1_1/"
    url<< "#{Cloudinary.config.cloud_name}/image/upload"
     
    form_tag(url, form_options) do
     content = []

      options.each do |name, value|
        content<< hidden_field_tag(name, value)
      end

      content<< capture(&block)

      content.join("\n").html_safe
    end
  end

  def save_image
    #save in the menu DB
    @menu = Menu.find(params[:menu])
    @menu.image_id = options[:signature]
    @menu.save
  end

  def valid_cloudinary_response?
    received_signature = request.query_parameters[:signature]
    calculated_signature = Cloudinary::Utils.api_sign_request \
    request.query_parameters.except(:signature),
    Cloudinary.config.api_secret
    received_signature == calculated_signature
  end
end
