class RestaurantsController < ApplicationController
  # GET /restaurants
  # GET /restaurants.json
  def index
    @restaurants = Restaurant.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @restaurants }
    end
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @restaurant = Restaurant.find(params[:id])
    @restaurant.geo_update_from_map = false 

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @restaurant }
    end
  end


def major_currencies(hash)
  hash.inject([]) do |array, (id, attributes)|
    priority = attributes[:priority]
    if priority && priority < 10
      array[priority] ||= []
      array[priority] << id
    end
    array
  end.compact.flatten
end

# Returns an array of all currency id
def all_currencies(hash)
  hash.keys
end

  # GET /restaurants/new
  # GET /restaurants/new.json
  def new
    @restaurant = Restaurant.new
    
    # preparing the form select for the currencies
     @currencies = [] 
     all_currencies(Money::Currency.table).each do |currency|  
       name = Money::Currency.table[currency][:name]
       iso_code = Money::Currency.table[currency][:iso_code]
       @currencies << [name, iso_code]
     end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @restaurant }
    end
  end

  # GET /restaurants/1/edit
  def edit
    @restaurant = Restaurant.find(params[:id])

    # preparing the form select for the currencies
     @currencies = [] 
     all_currencies(Money::Currency.table).each do |currency|  
       name = Money::Currency.table[currency][:name]
       iso_code = Money::Currency.table[currency][:iso_code]
       @currencies << [name, iso_code]
     end
  end

  #currently not used as this was written for the geocoder - 
  #perhaps if we solve the server lookup we can use this
  def update_geo_location (restaurant)
    if restaurant.street != "" && restaurant.city != "" && restaurant.country != ""
      searchString = restaurant.street+","+restaurant.city+","+restaurant.country
      #s = Geocoder.coordinates(searchString)
      s = Geocoder.coordinates(searchString)
      #if s
        restaurant.lat = s[0].to_f
        restaurant.lon = s[1].to_f
      #end
    end 
  end

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = Restaurant.new(params[:restaurant])
    @restaurant.geo_update_from_map = false

    update_geo_location (@restaurant)


    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render json: @restaurant, status: :created, location: @restaurant }
      else
        format.html { render action: "new" }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /restaurants/1
  # PUT /restaurants/1.json
  def update
    @restaurant = Restaurant.find(params[:id])

     #update_geo_location (@restaurant)
    @restaurant.geo_update_from_map = false  

    respond_to do |format|
      if @restaurant.update_attributes(params[:restaurant])
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy
    @restaurant = Restaurant.find(params[:id])
    @restaurant.destroy

    respond_to do |format|
      format.html { redirect_to restaurants_url }
      format.json { head :no_content }
    end
  end
end
