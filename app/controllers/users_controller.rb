class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :only_allow_admin, :only => [ :index ]

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end
  
  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user], :as => :admin)
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end

  def update_active_restaurant 
  @user = User.find(params[:id])  
  @user.activeRestaurant_id = params[:activeRestaurant_id] #if admin?  
  if @user.update_attributes(params[:user])  
    flash[:notice] = "Successfully updated active restaurant. @user.id @user.activeRestaurant_id"  
    redirect_to dashboard_path
  end  
end 
    
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end

end