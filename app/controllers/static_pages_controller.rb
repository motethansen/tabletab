class StaticPagesController < ApplicationController
  def home
  end

  def dashboard
  	@user = User.find(current_user.id)
  end

  def index
    @users = User.all
  end


  def statistics
  	@restaurant = Restaurant.all
  	@orders = Order.all
  	@orders_nfc = Order.where(:source => "nfc")
  	@orders_qr = Order.where(:source => "QR")

  	my_array =[]

  	@orders.each do |item|
  		diff = (item.close_order_timestamp).to_i - (item.create_order_timestamp).to_i
  		my_array << (diff)
  	end

  	@order_timemax = Time.at(my_array.max).utc.strftime("%H:%M:%S") #=> "01:00:00"
  	@order_timemin = Time.at(my_array.min).utc.strftime("%H:%M:%S") #=> "01:00:00"

  	total = my_array.inject(:+)
  	len = my_array.length
	average = total.to_f / len # to_f so we don't get an integer result
	sorted = my_array.sort
	median = len % 2 == 1 ? sorted[len/2] : (sorted[len/2 - 1] + sorted[len/2]).to_f / 2

  	@order_timeavg = Time.at(average).utc.strftime("%H:%M:%S") #=> "01:00:00"
  	
  end
  
end
