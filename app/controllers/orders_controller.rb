class OrdersController < ApplicationController
  helper_method :call_waiter
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
    @user = User.find(current_user.id)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @order }
    end
  end

  def show_table_for_order
      @table = Table.find(params[:table_id])
      @order = Order.new
 #     @menu = Menu.where(:restaurant_id => @table.restaurant_id, :displayonmobilemenu_id => true ).all
      @restaurant = Restaurant.find(@table.restaurant_id)


      render "orders", :layout => 'mobiledevices'
  end

  def action_order
      @order = Order.find(params[:order_id])
      
      if @order
        if @order.status == "2"
          @order.status = "4" # is completed as C
          @order.close_order_timestamp  = Time.zone.now.utc.iso8601()
        end
      end

      if @order.save
        redirect_to :action=>"index"
      end
  end

  def call_waiter
      
        @order = Order.new(params[:order])
        @order.created_at = Time.zone.now.utc.iso8601()
        @order.create_order_timestamp  = Time.zone.now.utc.iso8601()

        #When call waiter is activated the status 
        #send is CW. When its an Order its O.
        #On the server the status will be set to W when its received.
        if @order.status == "CW" #value send from mobile
          @order.status = "2"     #setting the status to Waiting
        end

        tableid = @order.table_id

        # lets see if its a QR code or NGC chip the cal came from
        # Create the URL to return to the same page
        # at the same time we detect how the URL was detected from the device QR or NFC
        if @order.source == "nfc"
          @url = url_for :controller => 'orders', :action => 'show_table_for_order', :table_id => tableid, :source => :nfc
        elsif @order.source == ""
          @url = url_for :controller => 'orders', :action => 'show_table_for_order', :table_id => tableid
          @order.source = "QR"
        end
        current_url =  @url
        
        if @order.lat != ""  
          #'http://0.0.0.0:3000/order/5'
          if @order.save
            respond_to do |format|
              format.html { redirect_to( current_url )}
              format.js { redirect_to( orders_url)}#{ render "orders", :layout=>'mobiledevices' } 
            end
          end
        end
  end
  
 # def new
 #      @order = Orders.new
 # end
  
 # def create
 #   @order = Orders.new(params[:table_id])
 #   if @order.save
      # flash[:success] = "Welcome to TABLETAB "
      #redirect_to root_url, :notice => "Signed up!" 
      #redirect_to @user
 #     gflash :notice => { :value => "The Order was saved in our database !", :time => 4000 } 
 #   else
 #     render "orders"
 #   end
 # end


  # GET /orders/new
  # GET /orders/new.json
  def new
    @order = Order.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(params[:order])

#    respond_to do |format|
#      if @order.save
#        format.html { redirect_to @order, notice: 'Order was successfully created.' }
#        format.json { render json: @order, status: :created, location: @order }
#      else
#        format.html { render action: "new" }
#        format.json { render json: @order.errors, status: :unprocessable_entity }
#      end
#    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])

    respond_to do |format|
      if @order.update_attributes(params[:order])
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_url }
      format.json { head :no_content }
    end
  end
end
