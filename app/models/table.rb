class Table < ActiveRecord::Base
  attr_accessible :restaurant_id, :tablename, :tablenumber

  belongs_to :restaurants
end
