class Restaurant < ActiveRecord::Base
  attr_accessible :city, :country, :display_menu_on_mobile, :name, :currency_id, :phone, :restaurant_size, :restaurant_message, :state, :street, :user_id, :zip, :lon, :lat, :geofence_radius
  
  attr_accessor :geo_update_from_map

  belongs_to :user
  validates :user_id, presence: true
  has_many :tables

 

# Geocoder is used with bing
  geocoded_by :address, :latitude  => :lat, :longitude => :lon  do |restaurant, results| # ActiveRecord
  	if @geo_update_from_map == false
      if geo = results.first
    	 restaurant.lat = geo.latitude
    	 restaurant.lon = geo.longitude
  	  end
    end
  end

  before_validation :geocode          # auto-fetch coordinates

def address
  [street, city, country].compact.join(', ')
end

def geo_update_from_map
  self.geo_update_from_map
end
#def initialize
#  @geo_update_from_map = false
#end

end
