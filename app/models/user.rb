class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #for admin role
  attr_accessible  :role_ids, :as => :admin
  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation,  :remember_me , :activeRestaurant_id, :time_zone
  # attr_accessible :title, :body
  attr_accessor :accessible

  validates_presence_of :name
  validates_uniqueness_of :name, :email, :case_sensitive => false

  has_many :restaurants, dependent: :destroy
  has_and_belongs_to_many :roles

 private  
   
  #def mass_assignment_authorizer  
  #  super + [:activeRestaurant_id]  
  #end 
  
  def mass_assignment_authorizer(role = :default)
    if accessible == :all
      self.class.protected_attributes
    else
      super + (accessible || [])
    end
  end

  #def mass_assignment_authorizer  
  #  if accessible == :all  
  #    self.class.protected_attributes  
  #  else  
  #    super + (accessible || [])  
  #  end  
  #end

end
