class Menu < ActiveRecord::Base
  attr_accessible :category, :description, :display_on_mobile_menu, :happy_hour_from, :happy_hour_price, :happy_hour_to, :image_id, :price, :restaurant_id, :title

# composed_of :price,
#    :class_name => "Money",
#    :mapping => [%w(cents cents), %w(currency currency_as_string)],
#    :constructor => Proc.new { |cents, currency| Money.new(cents || 0, currency ||  Money.default_currency) },
#    :converter => Proc.new { |value| value.respond_to?(:to_money) ? value.to_money : raise(ArgumentError, "Can't convert #{value.class} to Money") }
    

# Use model level currency
#  register_currency :dkk

  monetize :price, :with_currency => :gbp

end
