Tabletab::Application.routes.draw do

  #get "static_pages/home"
  #root to: 'static_pages#home'

  authenticated :user do
    root :to => 'static_pages#home'
    match '/dashboard', to: 'static_pages#dashboard'
    resources :restaurants
    resources :tables
    #resources :menus
   
  end
  root :to => "static_pages#home"
  match '/statistics', to: 'static_pages#statistics'
  
  #devise_for :users
  # replace devise_for :users with: for check on who can create users
  devise_for :users,  :controllers => { :registrations => "users/registrations" }

  resources :users do
    member do
      post 'update_active_restaurant'
    end
  end
  
  resources :menus do
    member do
      post 'save_image'
    end  
  end

  resources :orders #do
  #  member do
  #    put 'call_waiter'
  #  end
  #end
  match 'order/:table_id' => 'orders#show_table_for_order'
  #match 'order/:table_id', :to => 'orders#call_waiter'
  post "call_waiter" => "orders#call_waiter", :as => "call_waiter"

  put 'order/:order_id/order_action' => "orders#action_order", :as => "action_order" #:conditions => {:method => :put}

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
