class ChangeCurrencyTypeinRestaurant < ActiveRecord::Migration
  def change
  	change_column :restaurants, :currency_id, :string
  end
end
