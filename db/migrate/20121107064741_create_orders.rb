class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :restaurant_id
      t.integer :table_id
      t.string :status
      t.timestamp :create_order_timestamp
      t.timestamp :produce_order_timestamp
      t.timestamp :close_order_timestamp

      t.timestamps
    end
  end
end
