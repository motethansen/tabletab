class FixColumnNameOnRestaurantMessage < ActiveRecord::Migration
  def up
  	rename_column :restaurants, :restautant_message, :restaurant_message
  end

  def down
  	rename_column :restaurants, :restaurant_message, :restautant_message
  end
end
