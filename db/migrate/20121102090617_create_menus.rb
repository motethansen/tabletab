class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :title
      t.string :description
      t.string :catagory
      t.decimal :price
      t.integer :image_id
      t.time :happy_hour_from
      t.time :happy_hour_to
      t.decimal :happy_hour_price
      t.integer :restaurant_id
      t.boolean :display_on_mobile_menu

      t.timestamps
    end
  end
end
