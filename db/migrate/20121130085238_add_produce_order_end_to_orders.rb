class AddProduceOrderEndToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :produce_order_end, :timestamp
  end
end
