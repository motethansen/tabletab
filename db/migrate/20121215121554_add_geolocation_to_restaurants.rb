class AddGeolocationToRestaurants < ActiveRecord::Migration
  def change
    add_column :restaurants, :lon, :float
    add_column :restaurants, :lat, :float
    add_column :restaurants, :geofence_radius, :integer
  end
end
