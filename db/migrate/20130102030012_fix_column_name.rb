class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :restaurants, :franchise, :display_menu_on_mobile
  end
end
