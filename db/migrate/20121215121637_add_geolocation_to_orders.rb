class AddGeolocationToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :lon, :float
    add_column :orders, :lat, :float
  end
end
