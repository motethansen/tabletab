class FixColumnTypeOnRestaurant < ActiveRecord::Migration
  def change
  	change_column :restaurants, :restaurant_type, :text
  	rename_column :restaurants, :restaurant_type, :restautant_message
  end
end
