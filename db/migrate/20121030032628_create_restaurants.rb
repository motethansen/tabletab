class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.string :phone
      t.string :restaurant_size
      t.integer :number_of_staff_pr_shift
      t.string :restaurant_type
      t.boolean :franchise
      t.integer :user_id

      t.timestamps
    end
  end
end
