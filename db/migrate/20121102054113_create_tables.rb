class CreateTables < ActiveRecord::Migration
  def change
    create_table :tables do |t|
      t.integer :tablenumber
      t.string :tablename
      t.integer :restaurant_id

      t.timestamps
    end
  end
end
