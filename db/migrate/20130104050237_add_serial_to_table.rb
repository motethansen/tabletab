class AddSerialToTable < ActiveRecord::Migration
  def change
    add_column :tables, :serial, :integer, :limit => 8
  end
end
