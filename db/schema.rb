# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130128140001) do

  create_table "menus", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.string   "category"
    t.decimal  "price"
    t.integer  "image_id"
    t.time     "happy_hour_from"
    t.time     "happy_hour_to"
    t.decimal  "happy_hour_price"
    t.integer  "restaurant_id"
    t.boolean  "display_on_mobile_menu"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "orders", :force => true do |t|
    t.integer  "restaurant_id"
    t.integer  "table_id"
    t.string   "status"
    t.datetime "create_order_timestamp"
    t.datetime "produce_order_timestamp"
    t.datetime "close_order_timestamp"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.datetime "produce_order_end"
    t.string   "status_text"
    t.string   "source"
    t.float    "lon"
    t.float    "lat"
  end

  create_table "restaurants", :force => true do |t|
    t.string   "name"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "country"
    t.string   "phone"
    t.string   "restaurant_size"
    t.string   "currency_id"
    t.text     "restaurant_message",     :limit => 255
    t.boolean  "display_menu_on_mobile"
    t.integer  "user_id"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.float    "lon"
    t.float    "lat"
    t.integer  "geofence_radius"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "tables", :force => true do |t|
    t.integer  "tablenumber"
    t.string   "tablename"
    t.integer  "restaurant_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "serial",        :limit => 8
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",          :null => false
    t.string   "encrypted_password",     :default => "",          :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.string   "name"
    t.integer  "activeRestaurant_id"
    t.string   "time_zone",              :default => "Singapore"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
