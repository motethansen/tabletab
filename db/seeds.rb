# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts 'CREATING ROLES'
Role.delete_all
Role.create([
  { :name => 'superadmin' }, #tabletab superadmin - can assign other roles 
  { :name => 'admin' },      # a restaurants admin (owner)
  { :name => 'manager' },    # a restaurant mamager
  { :name => 'bar' },        # a bar manager
  { :name => 'kitchen' },    # a kitchen chef
  { :name => 'waiter' },     # a waiter
  { :name => 'trial' }       # a trial user (limited period)
], :without_protection => true)
#puts 'SETTING UP DEFAULT USER LOGIN'
#user = User.find(:first, :conditions => [ "email = ?", "michael@neslein-asia.com"])
#if user
#	user.add_role :superadmin
#else	
#	user = User.create! :name => 'Super Admin', :email => 'michael@neslein-asia.com', :password => 'Takeal00k!', :password_confirmation => 'Takeal00k!'
#	puts 'New user created: ' << user.name
#	user.add_role :superadmin
#end

#user1 = User.find(:first, :conditions => [ "email = ?", "michael@neslein-asia.com"])
#if user1
#	user1.add_role :superadmin
#else	
#	user1 = User.create! :name => 'SuperDuper Admin', :email => 'mikael@neslein-asia.com', :password => 'M!karu2012', :password_confirmation => 'M!karu2012'
#	puts 'New user created: ' << user2.name
#	user1.add_role :superadmin
#end
 
puts 'Fill empty currency - only run once !!!'

Restaurant.find_each do |res|
	
		res.update_attributes(:currency_id => 'SGD')

end
